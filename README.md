# Jyanken

## Overview

A test game created in Godot 4, inspired by a game of "rock-paper-scissors" played in the show Gaki no Tsukai during their 2015/16 "No-Laughing Scientists" special.

## Rules

The game will show you a card containing "Rock", "Paper" or "Scissors".  

Under that card it will tell you whether you have to "Win" or "Lose".  

You have to then pick one of your three cards in order to carry out that outcome.  

Rock beats Scissors, Paper beats Rock and Scissors beats Paper. Conversely, Scissors loses to Rock, Rock loses to Paper and Paper loses to Scissors.

You cannot draw.

Points increase with each correct click.
