extends Node2D

const ENEMY_CARD = false
const PLAYER_CARD = true

var player_options
var enemy_card: Node2D
var card_types = ["rock", "paper", "scissors"]
var win_mode = false

var points = 0

var win_map = {
	"rock": "scissors",
	"paper": "rock",
	"scissors": "paper"
}

var lose_map = {
	"rock": "paper",
	"paper": "scissors",
	"scissors": "rock"
}

func check_win(picked_card):
	
	# print("You: ", picked_card, " Them: ", enemy_card.card_type, " Map: ", win_map[picked_card] )
	
	if win_mode:
		return win_map[picked_card] == enemy_card.card_type
	else:
		return lose_map[picked_card] == enemy_card.card_type

func handle_card_press(card_type):
	var won: bool = check_win(card_type)
	if won: points += 1
	spawn_enemy_card()
	
func handle_card_release():
	print("Released from Level")

func spawn_enemy_card():
	
	if enemy_card: 
		enemy_card.queue_free()
	
	var card: PackedScene = preload("res://scenes/card.tscn")
	enemy_card = card.instantiate()
	add_child(enemy_card)
	enemy_card.set_card_params(card_types[randi() % card_types.size()], %EnemyCardMarker.position, ENEMY_CARD)
	
	win_mode = bool(randi() % 2)
	match win_mode:
		true:
			%GameModeLabel.texture = load("res://art/win.png")
		false:
			%GameModeLabel.texture = load("res://art/lose.png")
	
func spawn_player_cards():
	var card: PackedScene = preload("res://scenes/card.tscn")
	
	# Create player hand
	
	var player_rock = card.instantiate()
	var player_paper = card.instantiate()
	var player_scissors = card.instantiate()
	
	#var x_max = get_viewport_rect().size.x
	#var y_max = get_viewport_rect().size.y
	
	player_options = [player_rock, player_paper, player_scissors]
	
	for option in player_options:
		$PlayerHand.add_child(option)
		option.addButtonPressHandler(handle_card_press)
	
	player_rock.set_card_params("rock", %RockMarker.position, PLAYER_CARD)
	player_paper.set_card_params("paper", %PaperMarker.position, PLAYER_CARD)
	player_scissors.set_card_params("scissors", %ScissorsMarker.position, PLAYER_CARD)

# Called when the node enters the scene tree for the first time.
func _ready():
	spawn_player_cards()
	spawn_enemy_card()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	%PointsLabel.text = str(points)
