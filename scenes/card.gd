extends Node2D

signal pressed
signal released

var card_type = ""

func set_card_params(type, pos, is_player_card: bool):
	""" Takes a string "rock", "paper" or "scissors" and sets parameters for the 
	card based on the enum"""
	card_type = type
	match type:
		"rock":
			$Button.icon = load("res://art/rock.png")
		"scissors":
			$Button.icon = load("res://art/scissors.png")
		"paper":
			$Button.icon = load("res://art/paper.png")
			
	position = pos
	$Button.disabled = !is_player_card

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
 
func addButtonPressHandler(callable):
	pressed.connect(callable)
	
func addButtonReleaseHangler(callable):
	released.connect(callable)

func _on_button_pressed():
	pressed.emit(card_type)
